package com.features.venues

import android.arch.lifecycle.MutableLiveData
import com.core.platform.BaseViewModel
import com.features.venues.list.GetVenueList
import com.features.venues.list.VenueView
import com.features.venues.list.VenueViewMapper
import javax.inject.Inject

class VenueViewModel
@Inject constructor(private val getVenueList: GetVenueList,
                    private val venueViewMapper: VenueViewMapper) : BaseViewModel() {
    private var venueList: ArrayList<Venue> = arrayListOf()
    var venueViewList: MutableLiveData<List<VenueView>> = MutableLiveData()

    fun loadList() = getVenueList.execute({ it.either(::handleFailure, ::handleVenueListResponse) }, Unit)

    private fun handleVenueListResponse(venueList: List<Venue>) {
        updateVenueList(venueList)
        venueViewList.value = venueViewMapper.map(venueList)
    }

    private fun updateVenueList(venues: List<Venue>) {
        venueList.clear()
        venueList.addAll(venues)
    }
}