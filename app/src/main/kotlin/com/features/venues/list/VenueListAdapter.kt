package com.features.venues.list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.core.extension.inflate
import com.core.extension.loadResource
import com.core.extension.loadUrlImage
import com.example.testtechniquekoolicar.R
import kotlinx.android.synthetic.main.row_venue.view.*
import javax.inject.Inject
import kotlin.properties.Delegates

class VenueListAdapter
@Inject constructor() : RecyclerView.Adapter<VenueListAdapter.ViewHolder>() {
    internal var collection: List<VenueView> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(parent.inflate(R.layout.row_venue))

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) =
        viewHolder.bind(collection[position])

    override fun getItemCount() = collection.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(venueView: VenueView) {
            itemView.rowVenueName.text = venueView.name
            itemView.rowVenueAddress.text = venueView.address
            itemView.rowVenueImage.loadUrlImage(venueView.categoryUrlImage)
            itemView.rowArrowIcon.loadResource(android.R.drawable.ic_menu_send)
            itemView.setOnClickListener { venueClickListener.invoke(venueView) }
        }
    }

    lateinit var venueClickListener: (VenueView) -> Unit
}