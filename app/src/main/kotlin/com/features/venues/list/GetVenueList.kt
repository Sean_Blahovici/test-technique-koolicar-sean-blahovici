package com.features.venues.list

import com.core.constant.Constants.KOOLICAR_COORDINATES
import com.core.constant.Constants.QUERY
import com.core.constant.Constants.VENUE_LIST_ITEM_LIMIT
import com.core.failure.Failure
import com.core.functional.Either
import com.core.interactor.UseCase
import com.features.venues.Venue
import com.features.venues.VenueRepository
import javax.inject.Inject

/**
 * Contains application specific business-rules.
 * Hardcoding values for 10 restaurant venues nearest to Koolicar Montreal coordinates,
 * which are 45.5337892,-73.6223668.
 */
class GetVenueList @Inject constructor(private val venueRepository: VenueRepository) : UseCase<List<Venue>, Unit>() {

    override suspend fun run(params: Unit): Either<Failure, List<Venue>> =
            venueRepository.venueList(KOOLICAR_COORDINATES, QUERY, VENUE_LIST_ITEM_LIMIT)
}