package com.features.venues.list

import com.features.venues.Venue
import javax.inject.Inject

class VenueViewMapper @Inject constructor() {
    fun map(venueList: List<Venue>): List<VenueView> =
        venueList.map { mapVenueToView(it) }

    private fun mapVenueToView(venue: Venue): VenueView =
        VenueView(venue.name, venue.location.address, buildCategoryImageUrl(venue), VenueView.ClickData(venue.id))

    private fun buildCategoryImageUrl(venue: Venue): String {
        if (venue.categories.isEmpty()) return ""
        val icon = venue.categories[0].icon
        return icon.prefix + "64" + icon.suffix
    }
}
