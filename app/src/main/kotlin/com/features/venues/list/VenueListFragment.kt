package com.features.venues.list

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.core.constant.Constants.VENUE_ID
import com.core.extension.failure
import com.core.extension.observe
import com.core.extension.viewModel
import com.core.failure.Failure
import com.core.platform.BaseFragment
import com.example.testtechniquekoolicar.R
import com.features.venues.VenueViewModel
import kotlinx.android.synthetic.main.fragment_venue_list.*
import javax.inject.Inject

class VenueListFragment : BaseFragment() {
    override fun layoutId(): Int = R.layout.fragment_venue_list

    @Inject
    lateinit var venueListAdapter: VenueListAdapter

    private lateinit var venueViewModel: VenueViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        venueViewModel = viewModel(viewModelFactory) {
            observe(venueViewList, ::renderVenueList)
            failure(failure, ::handleFailure)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeView()
        showList()
        venueViewModel.loadList()
    }

    private fun initializeView() {
        venueListRV.layoutManager = LinearLayoutManager(activity)
        venueListRV.adapter = venueListAdapter
        buildVenueClickListener()
        venueListRetryButton.setOnClickListener { venueViewModel.loadList() }
    }

    private fun buildVenueClickListener() {
        venueListAdapter.venueClickListener = { venueView ->
            val bundle = Bundle()
            bundle.putString(VENUE_ID, venueView.clickData.venueId)
            mainNavigationController().navigate(R.id.action_VenueListFragment_to_venueDetailsFragment, bundle)
        }
    }

    private fun renderVenueList(venueList: List<VenueView>?) {
        showList()
        venueListAdapter.collection = venueList.orEmpty()
    }

    private fun showList() {
        venueListRV.visibility = View.VISIBLE
        venuesFailureGroup.visibility = View.GONE
    }

    private fun handleFailure(failure: Failure?) {
        when (failure) {
            is Failure.NetworkConnection -> renderFailure(R.string.failure_network_connection)
            is Failure.ServerError -> renderFailure(R.string.failure_server_error)
        }
    }

    private fun renderFailure(messageId: Int) {
        venueListRV.visibility = View.GONE
        venuesFailureGroup.visibility = View.VISIBLE
        venuesFailureLabel.text = getString(messageId)
    }
}