package com.features.venues.list

data class VenueView(val name: String, val address:String, val categoryUrlImage: String, val clickData: ClickData) {
    data class ClickData(val venueId: String)
}