package com.features.venues.details

data class VenueDetailsResponse(
    val response: Response
) {
    data class Response(
        val venue: VenueDetails
    )
}