package com.features.venues.details

import com.features.venues.Location

data class VenueDetails(
    val id: String,
    val name: String,
    val location: Location,
    val bestPhoto: BestPhoto,
    val contact: Contact
) {
    data class BestPhoto(
        val prefix: String,
        val suffix: String
    )

    data class Contact(val formattedPhone: String)

}