package com.features.venues.details

import com.core.failure.Failure
import com.core.functional.Either
import com.core.interactor.UseCase
import com.features.venues.VenueRepository
import javax.inject.Inject

class GetVenueDetails @Inject constructor(private val venueRepository: VenueRepository)
    : UseCase<VenueDetails, GetVenueDetails.Params>() {

    override suspend fun run(params: Params): Either<Failure, VenueDetails> =
            venueRepository.venueDetails(params.venueId)

    data class Params(val venueId: String)
}