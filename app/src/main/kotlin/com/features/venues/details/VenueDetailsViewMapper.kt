package com.features.venues.details

import javax.inject.Inject

class VenueDetailsViewMapper @Inject constructor() {
    fun map(venueDetails: VenueDetails): VenueDetailsView =
        VenueDetailsView(venueDetails.name, venueDetails.location.address,
            venueDetails.contact.formattedPhone, buildPhotoUrl(venueDetails.bestPhoto))

    private fun buildPhotoUrl(bestPhoto: VenueDetails.BestPhoto?): String {
        if (bestPhoto != null)
            return bestPhoto.prefix + "original" + bestPhoto.suffix
        return ""
    }
}