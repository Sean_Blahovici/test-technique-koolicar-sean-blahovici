package com.features.venues.details

import android.arch.lifecycle.MutableLiveData
import com.core.failure.Failure
import com.core.platform.BaseViewModel
import javax.inject.Inject

class VenueDetailsViewModel
@Inject constructor(private val getVenueDetails: GetVenueDetails,
                    private val venueDetailsViewMapper: VenueDetailsViewMapper) : BaseViewModel() {

    var selectedVenueDetails: MutableLiveData<VenueDetailsView> = MutableLiveData()

    fun loadVenueDetails(id: String?) =
        if (id != null)
            getVenueDetails.execute({ it.either(::handleFailure, ::handleVenueDetailsResponse) },
                GetVenueDetails.Params(id))
        else
            handleFailure(Failure.InternalError())

    private fun handleVenueDetailsResponse(venueDetails: VenueDetails) {
        selectedVenueDetails.value = venueDetailsViewMapper.map(venueDetails)
    }
}