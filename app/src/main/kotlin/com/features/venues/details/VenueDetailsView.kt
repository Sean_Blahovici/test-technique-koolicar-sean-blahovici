package com.features.venues.details

data class VenueDetailsView(val name: String, val address: String, val phoneNumber: String?, val photoUrl: String)