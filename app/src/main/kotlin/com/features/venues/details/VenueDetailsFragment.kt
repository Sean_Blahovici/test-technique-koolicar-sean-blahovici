package com.features.venues.details

import android.os.Bundle
import android.view.View
import com.core.constant.Constants
import com.core.extension.loadUrlImage
import com.core.extension.observe
import com.core.extension.viewModel
import com.core.failure.Failure
import com.core.platform.BaseFragment
import com.example.testtechniquekoolicar.R
import kotlinx.android.synthetic.main.fragment_venue_details.*

class VenueDetailsFragment : BaseFragment() {
    override fun layoutId(): Int = R.layout.fragment_venue_details

    private lateinit var venueDetailsViewModel: VenueDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        venueDetailsViewModel = viewModel(viewModelFactory) {
            observe(selectedVenueDetails, ::renderVenueDetails)
            observe(failure, ::handleFailure)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        loadVenueDetails()
    }

    private fun initView() {
        venueDetailsGroup.visibility = View.GONE
        venueDetailsFailureGroup.visibility = View.GONE
        venueDetailsRetryButton.setOnClickListener { loadVenueDetails() }
    }

    private fun loadVenueDetails() {
        val venueId = arguments?.getString(Constants.VENUE_ID)
        venueDetailsViewModel.loadVenueDetails(venueId)
    }

    private fun renderVenueDetails(venueDetailsView: VenueDetailsView?) {
        venueDetailsImage.loadUrlImage(venueDetailsView!!.photoUrl)
        venueDetailName.text = venueDetailsView.name
        venueDetailAddress.text = venueDetailsView.address
        venueDetailsPhone.text = venueDetailsView.phoneNumber

        venueDetailsGroup.visibility = View.VISIBLE
        venueDetailsFailureGroup.visibility = View.INVISIBLE
    }

    private fun handleFailure(failure: Failure?) {
        when (failure) {
            is Failure.NetworkConnection -> renderFailure(R.string.failure_network_connection)
            is Failure.ServerError -> renderFailure(R.string.failure_server_error)
        }
    }

    private fun renderFailure(messageId: Int) {
        venueDetailsGroup.visibility = View.GONE
        venueDetailsFailureGroup.visibility = View.VISIBLE
        venueDetailsFailureLabel.text = getString(messageId)
    }
}