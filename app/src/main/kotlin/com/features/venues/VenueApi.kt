package com.features.venues

import com.features.venues.details.VenueDetailsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.text.SimpleDateFormat
import java.util.*

interface VenueApi {
    companion object {
        private const val PARAM_LL = "ll"
        private const val PARAM_QUERY = "query"
        private const val PARAM_LIMIT = "limit"
        private const val PARAM_CLIENT_ID = "client_id"
        private const val PARAM_V = "v"
        private const val PARAM_CLIENT_SECRET = "client_secret"
        private const val SEARCH_FOR_VENUES = "venues/search"
        private const val PARAM_VENUE_ID = "PARAM_VENUE_ID"
        private const val VENUE_DETAILS = "venues/{$PARAM_VENUE_ID}"

        fun formattedDateForApi(): String = SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date())
    }

        @GET(SEARCH_FOR_VENUES) fun venueList(@Query(PARAM_CLIENT_ID) clientId: String,
                                              @Query(PARAM_CLIENT_SECRET) clientSecret: String,
                                              @Query(PARAM_V) v: String,
                                              @Query(PARAM_LL) ll: String,
                                              @Query(PARAM_QUERY) query: String,
                                              @Query(PARAM_LIMIT) limit: Int) : Call<VenueListResponse>

         @GET(VENUE_DETAILS) fun venueDetails(@Path(PARAM_VENUE_ID) venueId: String,
                                              @Query(PARAM_CLIENT_ID) clientId: String,
                                              @Query(PARAM_CLIENT_SECRET) clientSecret: String,
                                              @Query(PARAM_V) v: String) : Call<VenueDetailsResponse>
}