package com.features.venues

data class Venue(
    val id: String,
    val name: String,
    val location: Location,
    val categories: List<Category>
) {

    data class Category(
        val name: String,
        val icon: Icon
    ) {
        data class Icon(
            val prefix: String,
            val suffix: String
        )
    }
}