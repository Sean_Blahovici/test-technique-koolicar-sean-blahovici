package com.features.venues

data class Location(
    val distance: Int,
    val address: String
)