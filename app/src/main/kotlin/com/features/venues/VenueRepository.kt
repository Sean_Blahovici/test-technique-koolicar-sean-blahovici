package com.features.venues

import com.core.constant.Constants.FOURSQUARE_CLIENT_ID
import com.core.constant.Constants.FOURSQUARE_CLIENT_SECRET
import com.core.failure.Failure
import com.core.functional.Either
import com.core.platform.NetworkHandler
import com.features.venues.details.VenueDetails
import javax.inject.Inject

interface VenueRepository {
    fun venueList(latLng: String, query: String, limit: Int): Either<Failure, List<Venue>>
    fun venueDetails(venueId: String): Either<Failure, VenueDetails>

    class Foursquare @Inject constructor(private val venueApi: VenueApi,
                                         private val networkHandler: NetworkHandler) : VenueRepository {
        override fun venueList(latLng: String, query: String, limit: Int): Either<Failure, List<Venue>> {
            if (!networkHandler.isConnectedToInternet()) return Either.Left(Failure.NetworkConnection())
            try {
                val response = venueApi.venueList(FOURSQUARE_CLIENT_ID, FOURSQUARE_CLIENT_SECRET,
                    VenueApi.formattedDateForApi(), latLng, query, limit).execute()
                if (response.isSuccessful) {
                    response.body()?.let {
                        return Either.Right(it.response.venues)
                    }
                }
            } catch (e: Throwable) { }
            return Either.Left(Failure.ServerError())
        }

        override fun venueDetails(venueId: String): Either<Failure, VenueDetails> {
            if (!networkHandler.isConnectedToInternet()) return Either.Left(Failure.NetworkConnection())
            try {
                val response = venueApi.venueDetails(venueId, FOURSQUARE_CLIENT_ID, FOURSQUARE_CLIENT_SECRET,
                    VenueApi.formattedDateForApi()).execute()
                if (response.isSuccessful) {
                    response.body()?.let {
                        return Either.Right(it.response.venue)
                    }
                }
            } catch (e: Throwable) {}
            return Either.Left(Failure.ServerError())
        }
    }
}