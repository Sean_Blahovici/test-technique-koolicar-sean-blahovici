package com.features.venues

data class VenueListResponse(val response: Response) {
    data class Response(val venues: List<Venue>)
}
