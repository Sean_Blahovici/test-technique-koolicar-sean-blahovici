package com.core.platform

import android.graphics.Color
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.core.AndroidApplication
import com.core.di.ApplicationComponent
import com.example.testtechniquekoolicar.R
import kotlinx.android.synthetic.main.layout_main.*

class MainActivity : AppCompatActivity() {
    private val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (application as AndroidApplication).appComponent!!
    }

    private var parentLayout: DrawerLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
        setContentView(R.layout.layout_main)
        val host: NavHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment? ?: return
        val navController = host.navController
        setupActionBar(navController)
    }

    private fun setupActionBar(navController: NavController) {
        setSupportActionBar(toolBar)
        toolBar.title = title
        toolBar.setTitleTextColor(Color.WHITE)
        parentLayout = findViewById(R.id.activity_layout)
        NavigationUI.setupActionBarWithNavController(this, navController, parentLayout)
    }

    override fun onSupportNavigateUp(): Boolean =
        NavigationUI.navigateUp(Navigation.findNavController(this, R.id.nav_host_fragment), parentLayout)
}
