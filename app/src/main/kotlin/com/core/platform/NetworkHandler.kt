package com.core.platform

import android.content.Context
import com.core.extension.networkInfo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkHandler
@Inject constructor(private val context: Context) {
    private val isConnected get() = context.networkInfo?.isConnectedOrConnecting

    fun isConnectedToInternet(): Boolean {
        return isConnected != null && isConnected == true
    }
}