package com.core.platform

import android.arch.lifecycle.ViewModelProvider
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.core.AndroidApplication
import com.core.di.ApplicationComponent
import com.example.testtechniquekoolicar.R
import javax.inject.Inject

abstract class BaseFragment : Fragment() {
    abstract fun layoutId(): Int

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    val appComponent: ApplicationComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (activity?.application as AndroidApplication).appComponent!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(layoutId(), container, false)

    protected fun mainNavigationController() = Navigation.findNavController(activity!!, R.id.nav_host_fragment)
}