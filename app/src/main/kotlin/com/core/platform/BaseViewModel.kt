package com.core.platform

import android.arch.lifecycle.ViewModel
import com.core.failure.Failure

abstract class BaseViewModel : ViewModel() {
    var failure: SingleLiveEvent<Failure> = SingleLiveEvent()

    protected fun handleFailure(failure: Failure) {
        this.failure.value = failure
    }
}