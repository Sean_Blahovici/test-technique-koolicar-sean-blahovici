package com.core.di

import com.core.AndroidApplication
import com.core.di.viewmodel.ViewModelModule
import com.core.platform.MainActivity
import com.features.venues.details.VenueDetailsFragment
import com.features.venues.list.VenueListFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, ViewModelModule::class])
interface ApplicationComponent {
    fun inject(application: AndroidApplication)
    fun inject(mainActivity: MainActivity)
        fun inject(VenueListFragment: VenueListFragment)
        fun inject(venueDetailsFragment: VenueDetailsFragment)
}