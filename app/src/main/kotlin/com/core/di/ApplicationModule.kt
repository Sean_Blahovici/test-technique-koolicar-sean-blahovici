package com.core.di

import android.content.Context
import com.core.AndroidApplication
import com.features.venues.VenueApi
import com.features.venues.VenueRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: AndroidApplication) {
    @Provides @Singleton fun provideApplicationContext(): Context = application

    @Provides fun provideVenueRepository(foursquare: VenueRepository.Foursquare): VenueRepository = foursquare

    @Provides @Singleton fun provideVenueApi(): VenueApi {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.foursquare.com/v2/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(VenueApi::class.java)
    }
}