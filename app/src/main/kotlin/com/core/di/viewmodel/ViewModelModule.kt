package com.core.di.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.features.venues.VenueViewModel
import com.features.venues.details.VenueDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(VenueViewModel::class)
    abstract fun bindsVenueViewModel(venueViewModel: VenueViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VenueDetailsViewModel::class)
    abstract fun bindsVenueDetailsViewModel(venueDetailsViewModel: VenueDetailsViewModel): ViewModel
}