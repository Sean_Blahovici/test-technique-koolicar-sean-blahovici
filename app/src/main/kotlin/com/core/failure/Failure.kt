package com.core.failure

sealed class Failure: Throwable() {
    class NetworkConnection: Failure()
    class ServerError: Failure()
    class InternalError: Failure()
}