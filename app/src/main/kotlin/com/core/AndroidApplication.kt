package com.core

import android.app.Application
import com.bumptech.glide.request.target.ViewTarget
import com.core.di.ApplicationComponent
import com.core.di.ApplicationModule
import com.core.di.DaggerApplicationComponent
import com.example.testtechniquekoolicar.R

class AndroidApplication : Application() {
    var appComponent: ApplicationComponent? = null

    override fun onCreate() {
        super.onCreate()
        initApplicationComponent()
        injectMembers()
        initGlide()
    }

    private fun initApplicationComponent() {
        if (appComponent == null) {
            appComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
        }
    }

    private fun injectMembers() = appComponent!!.inject(this)

    private fun initGlide() = ViewTarget.setTagId(R.id.glide_tag)
}