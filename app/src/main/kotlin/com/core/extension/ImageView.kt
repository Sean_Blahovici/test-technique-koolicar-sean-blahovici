package com.core.extension

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.core.glide.GlideApp

fun ImageView.loadUrlImage(url: String): Target<Drawable> =
    GlideApp.with(this.context.applicationContext)
        .load(url)
        .transition(DrawableTransitionOptions.withCrossFade())
        .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
        .apply(RequestOptions().centerCrop())
        .into(this)

fun ImageView.loadResource(resourceId: Int): Target<Drawable> = Glide.with(this.context.applicationContext)
    .load(resourceId)
    .transition(DrawableTransitionOptions.withCrossFade())
    .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
    .apply(RequestOptions().placeholder(resourceId))
    .apply(RequestOptions().centerCrop())
    .into(this)