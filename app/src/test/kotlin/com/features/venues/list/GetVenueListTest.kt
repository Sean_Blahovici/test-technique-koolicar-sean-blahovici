package com.features.venues.list

import TestData.testVenueList
import com.core.functional.Either
import com.features.venues.VenueRepository
import com.nhaarman.mockitokotlin2.*
import com.testConfig.UnitTest
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Before
import org.junit.Test

class GetVenueListTest : UnitTest() {
    private lateinit var getVenueList: GetVenueList
    private val mockVenueRepository: VenueRepository = mock()
    private val repositoryResponse = testVenueList()

    @Before
    fun setUp() {
        given { mockVenueRepository.venueList(any(), any(), any()) }
            .willReturn(Either.Right(repositoryResponse))
        getVenueList = GetVenueList(mockVenueRepository)
    }

    @Test
    fun `should get data from repository`() {
        val response = runBlocking { getVenueList.run(Unit) }

        response shouldBeInstanceOf  Either.Right(repositoryResponse)::class.java
        verify(mockVenueRepository).venueList(any(), any(), any())
        verifyNoMoreInteractions(mockVenueRepository)
    }
}