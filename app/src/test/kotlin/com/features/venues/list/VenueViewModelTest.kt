package com.features.venues.list

import TestData.CHEZ_LIEN_NAME
import TestData.MCDONALDS_NAME
import TestData.testVenueList
import android.arch.lifecycle.Observer
import com.core.failure.Failure
import com.core.functional.Either
import com.features.venues.Venue
import com.features.venues.VenueListResponse
import com.features.venues.VenueViewModel
import com.nhaarman.mockitokotlin2.*
import com.testConfig.AndroidTest
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBe
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class VenueViewModelTest : AndroidTest() {
    private lateinit var viewModel: VenueViewModel
    private val mockGetVenueList: GetVenueList = mock()
    private val mockVenueViewMapper: VenueViewMapper = VenueViewMapper()
    private val venueViewListObserver: Observer<List<VenueView>> = mock()
    private val failureObserver: Observer<Failure> = mock()

    @Before
    fun setup() {
        viewModel = VenueViewModel(mockGetVenueList, mockVenueViewMapper)
        viewModel.venueViewList.observeForever(venueViewListObserver)
        viewModel.failure.observeForever(failureObserver)
    }

    @Test
    fun `given get venue list succeeds should invoke venue list live data`() {
        `given get venue list succeeds and answers with`(testVenueList())

        `load venue list`()

        `assert venue view list observer invoked with correct test data`()
    }

    @Test
    fun `given no network connection when loading venue list should invoke failure live data`() {
        `given get venue list fails and answers with`(Failure.NetworkConnection())

        `load venue list`()

        `assert failure live data observer invoked with network connection failure`()
    }

    private fun `given get venue list succeeds and answers with`(venueList: List<Venue>) {
        Mockito.`when`(mockGetVenueList.execute(any(), any())).thenAnswer { answer ->
            answer.getArgument<(Either<Failure, List<Venue>>) -> Unit>(0)(Either.Right(venueList))
        }
    }

    private fun `given get venue list fails and answers with`(networkConnection: Failure.NetworkConnection) {
        Mockito.`when`(mockGetVenueList.execute(any(), any())).thenAnswer { answer ->
            answer.getArgument<(Either<Failure, VenueListResponse>) -> Unit>(0)(Either.Left(networkConnection))
        }
    }

    private fun `load venue list`() = runBlocking { viewModel.loadList() }

    private fun `assert venue view list observer invoked with correct test data`() {
        argumentCaptor<List<VenueView>>().apply {
            verify(venueViewListObserver, times(1)).onChanged(capture())
            val capturedVenueViewList = firstValue
            capturedVenueViewList.size shouldBe 2
            capturedVenueViewList[0].name shouldBe CHEZ_LIEN_NAME
            capturedVenueViewList[1].name shouldBe MCDONALDS_NAME
        }
    }

    private fun `assert failure live data observer invoked with network connection failure`() {
        argumentCaptor<Failure>().apply {
            verify(failureObserver, times(1)).onChanged(capture())
            firstValue shouldBeInstanceOf Failure.NetworkConnection::class.java
        }
    }
}