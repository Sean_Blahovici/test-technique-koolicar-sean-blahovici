package com.features.venues

import TestData.CHEZ_LIEN_ID
import TestData.MCDONALDS_ID
import TestData.testVenueList
import com.core.constant.Constants.KOOLICAR_COORDINATES
import com.core.constant.Constants.QUERY
import com.core.constant.Constants.VENUE_LIST_ITEM_LIMIT
import com.core.failure.Failure
import com.core.functional.Either
import com.core.platform.NetworkHandler
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.testConfig.AndroidTest
import junit.framework.TestCase.fail
import org.amshove.kluent.shouldBeInstanceOf
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test
import retrofit2.Call
import retrofit2.Response

class VenueRepositoryTest : AndroidTest() {
    private lateinit var repository : VenueRepository
    private val mockVenueApi: VenueApi = mock()
    private val mockNetworkHandler: NetworkHandler = mock()
    private val mockVenueListCall: Call<VenueListResponse> = mock()
    private val mockResponse: Response<VenueListResponse> = mock()
    private val mockVenueListResponse: VenueListResponse = mock()

    @Before fun setup() {
        given(mockVenueApi.venueList(any(), any(), any(), any(), any(), any())).willReturn(mockVenueListCall)
        given(mockVenueListCall.execute()).willReturn(mockResponse)
        repository = VenueRepository.Foursquare(mockVenueApi, mockNetworkHandler)
    }

    @Test fun `given connected when calling venue list repository should return venue list`() {
        given(mockNetworkHandler.isConnectedToInternet()).willReturn(true)
        `given api responds with test venue list`()

        val result = repository.venueList(KOOLICAR_COORDINATES, QUERY, VENUE_LIST_ITEM_LIMIT)

        `assert results match venue test list`(result)
    }

    @Test fun `given no network connection when calling venue list repository should return network connection failure`() {
        given(mockNetworkHandler.isConnectedToInternet()).willReturn(false)

        val result = repository.venueList(KOOLICAR_COORDINATES, QUERY, VENUE_LIST_ITEM_LIMIT)

        result shouldBeInstanceOf Either.Left(Failure.NetworkConnection())::class.java
    }

    private fun `given api responds with test venue list`() {
        given(mockResponse.isSuccessful).willReturn(true)
        given(mockResponse.body()).willReturn(mockVenueListResponse)
        val mockResponse: VenueListResponse.Response = mock()
        given(mockVenueListResponse.response).willReturn(mockResponse)
        given(mockResponse.venues).willReturn(testVenueList())
    }

    private fun `assert results match venue test list`(result: Either<Failure, List<Venue>>) {
        result.either({ fail() }, {
            it.size shouldEqual 2
            it[0].id shouldEqual CHEZ_LIEN_ID
            it[1].id shouldEqual MCDONALDS_ID
        })
    }
}