package com.features.venues.details

import TestData.CHEZ_LIEN_ID
import com.core.functional.Either
import com.features.venues.VenueRepository
import com.nhaarman.mockitokotlin2.*
import com.testConfig.UnitTest
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Before
import org.junit.Test

class GetVenueDetailsTest : UnitTest() {
    private lateinit var getVenueDetails: GetVenueDetails
    private val mockVenueRepository: VenueRepository = mock()
    private val repositoryResponse : VenueDetails = mock()

    @Before
    fun setUp() {
        given { mockVenueRepository.venueDetails(any()) }
            .willReturn(Either.Right(repositoryResponse))
        getVenueDetails = GetVenueDetails(mockVenueRepository)
    }

    @Test
    fun `should get data from repository`() {
        val response = runBlocking { getVenueDetails.run(GetVenueDetails.Params(CHEZ_LIEN_ID)) }

        response shouldBeInstanceOf  Either.Right(repositoryResponse)::class.java
        verify(mockVenueRepository).venueDetails(eq(CHEZ_LIEN_ID))
        verifyNoMoreInteractions(mockVenueRepository)
    }
}