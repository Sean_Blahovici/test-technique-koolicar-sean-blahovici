package com.features.venues.details

import TestData.CHEZ_LIEN_ID
import TestData.CHEZ_LIEN_NAME
import TestData.chezLienDetails
import android.arch.lifecycle.Observer
import com.core.failure.Failure
import com.core.functional.Either
import com.features.venues.VenueListResponse
import com.nhaarman.mockitokotlin2.*
import com.testConfig.AndroidTest
import kotlinx.coroutines.runBlocking
import org.amshove.kluent.shouldBeInstanceOf
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class VenueDetailsViewModelTest : AndroidTest() {
    private lateinit var viewModel: VenueDetailsViewModel

    private val mockGetVenueDetails: GetVenueDetails = mock()
    private val mockVenueDetailsViewMapper: VenueDetailsViewMapper = VenueDetailsViewMapper()
    private val venueDetailsViewObserver: Observer<VenueDetailsView> = mock()
    private val failureObserver: Observer<Failure> = mock()

    @Before
    fun setup() {
        viewModel = VenueDetailsViewModel(mockGetVenueDetails, mockVenueDetailsViewMapper)
        viewModel.selectedVenueDetails.observeForever(venueDetailsViewObserver)
        viewModel.failure.observeForever(failureObserver)
    }

    @Test
    fun `given get venue list succeeds should invoke venue list live data`() {
        `given get venue details succeeds and answers with`(chezLienDetails())

        `load venue details`(CHEZ_LIEN_ID)

        `assert venue view list observer invoked with correct test data`()
    }

    @Test
    fun `given no network connection when loading venue list should invoke failure live data`() {
        `given get venue list fails and answers with`(Failure.NetworkConnection())

        `load venue details`(CHEZ_LIEN_ID)

        `assert failure live data observer invoked with network connection failure`()
    }

    private fun `given get venue details succeeds and answers with`(venueDetails: VenueDetails) {
        Mockito.`when`(mockGetVenueDetails.execute(any(), eq(GetVenueDetails.Params(CHEZ_LIEN_ID)))).thenAnswer { answer ->
            answer.getArgument<(Either<Failure, VenueDetails>) -> Unit>(0)(Either.Right(venueDetails))
        }
    }

    private fun `given get venue list fails and answers with`(networkConnection: Failure.NetworkConnection) {
        Mockito.`when`(mockGetVenueDetails.execute(any(), any())).thenAnswer { answer ->
            answer.getArgument<(Either<Failure, VenueListResponse>) -> Unit>(0)(Either.Left(networkConnection))
        }
    }

    private fun `load venue details`(id : String) = runBlocking { viewModel.loadVenueDetails(id) }

    private fun `assert venue view list observer invoked with correct test data`() {
        argumentCaptor<VenueDetailsView>().apply {
            verify(venueDetailsViewObserver, times(1)).onChanged(capture())
            firstValue.name shouldEqual CHEZ_LIEN_NAME
        }
    }

    private fun `assert failure live data observer invoked with network connection failure`() {
        argumentCaptor<Failure>().apply {
            verify(failureObserver, times(1)).onChanged(capture())
            firstValue shouldBeInstanceOf Failure.NetworkConnection::class.java
        }
    }
}