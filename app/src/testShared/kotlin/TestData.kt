import com.features.venues.Location
import com.features.venues.Venue
import com.features.venues.details.VenueDetails

object TestData {
    fun testVenueList(): List<Venue> =
        listOf(chezLien(), mcDonalds())

    private fun chezLien(): Venue = Venue(
        CHEZ_LIEN_ID,
        CHEZ_LIEN_NAME,
        chezLienLocation(),
        listOf(
            Venue.Category(
                CHEZ_LIEN_CATEGORY_NAME,
                Venue.Category.Icon(CHEZ_LIEN_ICON_PREFIX, CHEZ_LIEN_ICON_SUFFIX)
            )
        )
    )

    private fun chezLienLocation() = Location(CHEZ_LIEN_DISTANCE, CHEZ_LIEN_ADDRESS)

    fun chezLienDetails() = VenueDetails(
        CHEZ_LIEN_ID, CHEZ_LIEN_NAME, chezLienLocation(),
        VenueDetails.BestPhoto(
            CHEZ_LIEN_IMAGE_PREFIX,
            CHEZ_LIEN_IMAGE_SUFFIX
        ), VenueDetails.Contact(CHEZ_LIEN_PHONE)
    )

    private fun mcDonalds(): Venue = Venue(
        MCDONALDS_ID,
        MCDONALDS_NAME,
        Location(MCDONALDS_DISTANCE, MCDONALDS_ADDRESS),
        listOf(
            Venue.Category(
                MCDONALDS_CATEGORY_NAME,
                Venue.Category.Icon(MCDONALDS_ICON_PREFIX, MCDONALDS_ICON_SUFFIX)
            )
        )
    )

    const val CHEZ_LIEN_ID = "SYBYSbSJB43243BEHB"
    const val CHEZ_LIEN_NAME = "Chez Lien"
    private const val CHEZ_LIEN_DISTANCE = 100
    private const val CHEZ_LIEN_ADDRESS = "3456 St-Urbain"
    private const val CHEZ_LIEN_CATEGORY_NAME = "Asian Restaurant"
    private const val CHEZ_LIEN_PHONE = "514-731-3426"
    private const val CHEZ_LIEN_ICON_PREFIX = "https://igx.4sqi.net/img/general/"
    private const val CHEZ_LIEN_ICON_SUFFIX = "/5163668_xXFcZo7sU8aa1ZMhiQ2kIP7NllD48m7qsSwr1mJnFj4.jpg"
    private const val CHEZ_LIEN_IMAGE_PREFIX = "https://igx.4sqi.net/img/general/"
    private const val CHEZ_LIEN_IMAGE_SUFFIX = "/5163668_xXFcZo7sU8aa1ZMhiQ2kIP7NllD48m7qsSwr1mJnFj4.jpg"

    const val MCDONALDS_ID = "dshbfh327gc3"
    const val MCDONALDS_NAME = "McDonalds"
    private const val MCDONALDS_DISTANCE = 300
    private const val MCDONALDS_ADDRESS = "36 St-Hubert"
    private const val MCDONALDS_CATEGORY_NAME = "Fast Food Restaurant"
    private const val MCDONALDS_ICON_PREFIX = "https://igx.4sqi.net/img/general/"
    private const val MCDONALDS_ICON_SUFFIX = "/5163668_xXFcZo7sU8aa1ZMhiQ2kIP7NllD48m7qsSwr1mJnFj4.jpg"
}
